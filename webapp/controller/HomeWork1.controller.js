sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/m/MessageToast",
	"sap/ui/model/resource/ResourceModel",
	"sap/ui/core/routing/History"
], function(Controller,MessageToast,ResourceModel,History) {
	"use strict";

	return Controller.extend("ManifestDemoCase.controller.HomeWork1", {
       onClickz: function(){
          var oBundle = new ResourceModel({
            bundleName: "ManifestDemoCase.i18n.i18n"
          });
          
       	  MessageToast.show(oBundle.getProperty("helloWorld"));
       },
		onBack : function () {
			var sPreviousHash = History.getInstance().getPreviousHash();

			//The history contains a previous entry
			if (sPreviousHash !== undefined) {
				window.history.go(-1);
			} else {
				// There is no history!
				// replace the current hash with page 1 (will not add an history entry)
				this.getOwnerComponent().getRouter().navTo("page1", null, true);
			}
		}
	});
});