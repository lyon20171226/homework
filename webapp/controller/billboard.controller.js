sap.ui.define([
	"sap/m/Input",
	"sap/m/Button",
	"sap/m/Dialog",
	"sap/m/List",
	"sap/m/StandardListItem",
	"sap/ui/core/mvc/Controller"
], function(Input,Button,Dialog,List,StandardListItem,Controller) {
	"use strict";
    
	return Controller.extend("ManifestDemoCase.controller.billboard", {
		onInit: function() {
	      //this.router = sap.ui.core.UIComponent.getRouterFor(this);	
          var oTable = this.getView().byId("showData");
          var oModel = new sap.ui.model.json.JSONModel();
          oModel.loadData('model/oModel.json');
          oTable.setModel(oModel);
          var oCount = 0;
          $.ajax({
              type: 'GET',         
              url : 'model/oModel.json',
              async : false,
              dataType: 'json',
              success: function(data,textStatus,jqXHR) {
              	oCount = data["Demo"].length; 
              }
          });
          var oText = "目前共有"+ oCount +"筆 正在發佈的公告";
          this.getView().byId("showCount").setText(oText);
          /*
          var oDataQ = new sap.ui.model.odata.v4.ODataModel({
            groupId : "$direct",
            synchronizationMode : "None",
            serviceUrl : "http://services.odata.org/TripPinRESTierService/"
          });
          */
		},
		onGotOOrgTree: function() {
			var app = new sap.m.app(function(){
				
			});
			app.to("billboard");
		},
		onPress: function() {
		  jQuery.sap.require("sap.m.MessageBox");	
          sap.m.MessageBox.information("此功能暫時不開放!", {                     
              title: "維修中"
          });
		},
		onDialogPress: function () {
			if (!this.pressDialog) {
                this.pressDialog = sap.ui.xmlfragment("newtypenewtype.view.AddBillboardMessage",sap.ui.controller("newtypenewtype.controller.AddBillboardMessage"));
				this.getView().addDependent(this.pressDialog);
			}

			this.pressDialog.open();
		}	
	});
});