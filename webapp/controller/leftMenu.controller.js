sap.ui.define([
	"sap/m/Input",
	"sap/m/Button",
	"sap/m/Dialog",
	"sap/m/List",
	"sap/m/StandardListItem",
	"sap/ui/core/mvc/Controller"
], function(Input,Button,Dialog,List,StandardListItem,Controller) {
	"use strict";
    
	return Controller.extend("ManifestDemoCase.controller.leftMenu", {
		onInit: function() {
          this._oRouter = sap.ui.core.UIComponent.getRouterFor(this);
          //var oTable = this.getView().byId("leftMenu");
          var oModel = new sap.ui.model.json.JSONModel();
          oModel.loadData('model/oMenu.json');
          this.getView().setModel(oModel);
		},
		onItemPress: function(oEvent){
		  var oObject = oEvent.getSource().getBindingContext();
		  var oItem = oObject.getModel().getProperty(oObject.getPath());
		  this._oRouter.navTo(oItem.itemValue, {
			  viewName: oItem.itemValue
		  });
		  console.log(oItem.itemValue);
		}
	});
});