sap.ui.define([
	"sap/m/Input",
	"sap/m/Button",
	"sap/m/Dialog",
	"sap/m/List",
	"sap/m/StandardListItem",
	"sap/ui/core/mvc/Controller",
	'sap/m/MessageBox',
	"sap/ui/core/routing/History"
], function(Input, Button, Dialog, List, StandardListItem, Controller, MessageBox,History) {
	"use strict";

	return Controller.extend("ManifestDemoCase.controller.HomeWork2", {
		onInit: function() {

		},
		onOpenInfo: function() {
			MessageBox.confirm(
				"這是個MessageBox", {
					styleClass: "sapUiSizeCompact"
				}
			);
		},

		onBack : function () {
			var sPreviousHash = History.getInstance().getPreviousHash();

			//The history contains a previous entry
			if (sPreviousHash !== undefined) {
				window.history.go(-1);
			} else {
				// There is no history!
				// replace the current hash with page 1 (will not add an history entry)
				this.getOwnerComponent().getRouter().navTo("page1", null, true);
			}
		}
	});
});