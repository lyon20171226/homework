sap.ui.define([
    "sap/ui/core/routing/History",	
	"sap/ui/core/mvc/Controller"
], function(History,Controller) {
	"use strict";
    
	return Controller.extend("ManifestDemoCase.controller.showView", {
		onInit: function() {
            this.getOwnerComponent().getRouter().getRoute("showView").attachPatternMatched(this._onRouteMatched, this);
            //this.router.getRoute("SecondView").attachMatched(this._onRouteMatched, this);
		},
		_onRouteMatched: function(oEvent) {
			var viewName = oEvent.getParameter("arguments").viewName;
			//var productId = oEvent.getParameter("arguments").productId;
			//this.getView().bindElement("/orders/" + viewName + "/products/"+productId);
			
		},
		onNavBack : function () {
			var sPreviousHash = History.getInstance().getPreviousHash();
            
			//The history contains a previous entry
			if (sPreviousHash !== undefined) {
				window.history.go(-1);
			} else {
				// There is no history!
				// replace the current hash with page 1 (will not add an history entry)
				this.getOwnerComponent().getRouter().navTo("leftMenu", null, true);
			}
		}
	});
});