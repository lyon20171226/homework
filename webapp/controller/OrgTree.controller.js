function addSelectedFlag(aNodes, bSelected) {
 	jQuery.each(aNodes, function(iIndex, oNode) {
  	oNode.selected = bSelected;
    if (oNode.nodes) {
    	addSelectedFlag(oNode.nodes, bSelected);
    }
  });
 }

sap.ui.define([
	"sap/m/Tree",
	"sap/m/StandardListItem",
	"sap/ui/core/mvc/Controller"
], function(Tree,StandardListItem,Controller) {
	"use strict";
    
	return Controller.extend("ManifestDemoCase.controller.OrgTree", {
		onInit: function() {
			var oModel = new sap.ui.model.json.JSONModel();
			var oTable = this.getView().byId("Tree");
			oModel.loadData('model/oOrgTree.json');
			oTable.setModel(oModel);
			
			var oModel1 = new sap.ui.model.json.JSONModel();
			var oTable1 = this.getView().byId("showData");
			oModel1.loadData('model/nullM.json');
		    oTable1.setModel(oModel1);
		},
		onShow: function(oEvent) {
     	var aItems = oEvent.getParameter("listItems") || [];
     	var myValue;
     	var	bSelected;
        jQuery.each(aItems, function(iIndex, oItem) {
      		var oNode = oItem.getBindingContext().getObject();
          	bSelected = oItem.getSelected();
            //oModel1.setData(sap.ui.getCore().getElementById('showdata').GetModel());
            /*$.ajax({
                type: 'GET',         
                url : 'model/nullM.json',
                async : false,
                dataType: 'json',
                success: function(data,textStatus,jqXHR) {
              	  oModel1.setData(data); 
                }
            });*/
            //馬拉戈壁的，loadData方法是一個同步請求，害我一直以為是我打錯，改用ajax啦~
            //oModel1.loadData('model/nullM.json');
            myValue = oNode.text;
            if (oNode.nodes) {
          		addSelectedFlag(oNode.nodes, bSelected);
            }
         });
        if(bSelected === true){
     	var oModel1 = new sap.ui.model.json.JSONModel();
     	oModel1 = this.getView().byId("showData").getModel();
        var aEmployees = oModel1.getProperty("/Demo");
        var oNewEmployee = {
          "appClass": myValue
        };
        aEmployees.push(oNewEmployee);
        oModel1.setProperty("/Demo", aEmployees);
        //console.log(oModel1.getProperty("/Demo"));
         console.log(this.getView().byId("showData"));
         oModel1.refresh();
        } 
		},
		onApply:function(){
			
			
		}
	});
});